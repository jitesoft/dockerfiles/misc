FROM registry.gitlab.com/jitesoft/dockerfiles/docker/buildx:latest
RUN apk add --no-cache curl libseccomp-dev coreutils git wget gnupg gcc openssl grep ca-certificates linux-headers jq \
 && wget https://gist.githubusercontent.com/Johannestegner/093e8053eabd795ed84b83e9610aed6b/raw/helper.sh -O /usr/local/bin/helper \
 && chmod +x /usr/local/bin/helper
